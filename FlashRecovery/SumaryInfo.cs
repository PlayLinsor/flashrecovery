﻿using System;
using System.Windows.Forms;

namespace FlashRecovery
{
    public partial class SumaryInfo : Form
    {

        public SumaryInfo()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        public void Log(string Text)
        {
            if (richTextBox1.InvokeRequired)
            {
                richTextBox1.Invoke(new Action<string>((s) => richTextBox1.Text += s+"\r\n"), Text);
            }
            else richTextBox1.Text += Text + "\r\n";

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void SumaryInfo_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }
    }
}
