﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace FlashRecovery
{
    public partial class MainForm : Form
    {

        SumaryInfo SI = new SumaryInfo();             // Форма с отсчетом 
        Recovery fRecovery = new Recovery();          // Класс Востановления
        

        public MainForm()
        {
            fRecovery.OnProgressChange += fRecovery_OnProgressChange;
            fRecovery.OnFileAlert += fRecovery_OnFileAlert;
            fRecovery.OnComplite += fRecovery_OnComplite;

            InitializeComponent();
        }

        void fRecovery_OnComplite()
        {
            toolStripProgressBar1.Visible = false;
            toolStripStatusLabel1.Text = "Готово";
        }

        void fRecovery_OnFileAlert(string Path)
        {
            SI.Log(Path);
            
        }

        void fRecovery_OnProgressChange(int Procent)
        {
            toolStripProgressBar1.Value = Procent;
        }

        void fRecovery_OnFileAlert()
        {
            Text = "Ok";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string dir = comboBox1.Text;
            string[] Files = Directory.GetFiles(dir, "*", SearchOption.AllDirectories);
            FileInfo fi;
            FileAttributes Fa;

            foreach (string item in Files)
            {
                fi = new FileInfo(item);
                Fa = fi.Attributes;
                fi.Attributes = FileAttributes.Normal;
                
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            toolStripProgressBar1.Visible = true;
            fRecovery.StartRecovery(comboBox1.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SI.Location = new Point(this.Location.X + this.Size.Width, this.Location.Y);
            SI.Show();
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            
           
        }
    }
}
