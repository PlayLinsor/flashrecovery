﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.ComponentModel;

namespace FlashRecovery
{
    class Recovery
    {
        
        private string TargetPath = "";                              // Путь проверки.
        private BackgroundWorker Worker = new BackgroundWorker();    // Фоновая проверка
        
        // Атрибуты файлов
        private FileInfo fInfo;           
        private FileAttributes fAtribyte;

        // Подозрительный файл
        public delegate void onFileAlert(string Path);
        public event onFileAlert OnFileAlert;

        // Изменение прогресса проверки
        public delegate void onProgressChange(int Procent);
        public event onProgressChange OnProgressChange;

        // Проверка завершена
        public delegate void onComplite();
        public event onComplite OnComplite;


        public Recovery()
        {
            Worker.DoWork += _Worker_DoWork;
            Worker.RunWorkerCompleted += _Worker_RunWorkerCompleted;
            Worker.ProgressChanged += _Worker_ProgressChanged;
        }

        /// <summary>
        /// Процедура обработки файловой системой 
        /// </summary>
        /// <param name="Path">Путь об востановления</param>
        public void StartRecovery(string Path)
        {
            TargetPath = Path;
            Worker.WorkerReportsProgress = true;
            Worker.RunWorkerAsync(); 
        }

        // Переадресация кому нужно
        private void _Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            OnProgressChange(e.ProgressPercentage);
        }

        // Проверка завершена
        private void _Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            OnComplite();
        }

        // Процедура обработки
        private void _Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            Worker.ReportProgress(0);
            int cProgess = 0;
            int cPos = 0;
            string[] Files = Directory.GetFiles(this.TargetPath, "*", SearchOption.AllDirectories);
            int max = Files.Length;

            foreach (string item in Files)
            {
                if (++cPos * 100 / max != cProgess)
                {
                    cProgess = cPos * 100 / max;
                    Worker.ReportProgress(cProgess);
                }

                _FileFilter(item);
                
                fInfo = new FileInfo(item);
                fAtribyte = fInfo.Attributes;
                fInfo.Attributes = FileAttributes.Normal;
            }

            Worker.ReportProgress(100);
        }

        // Фильтр файлов
        private void _FileFilter(string FilePath)
        {
            OnFileAlert.Invoke(FilePath);
           // OnFileAlert.BeginInvoke(FilePath, null, null);
            //OnFileAlert(FilePath);
        }
    }
}
